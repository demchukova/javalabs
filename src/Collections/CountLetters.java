//16.14 (Counting Letters) Modify the program of Fig. 16.18 to count the number of occurrences
//        of each letter rather than of each word. For example, the string "HELLO THERE" contains two H s, three
//        E s, two L s, one O , one T and one R . Display the results.
package Collections;

import java.util.*;

public class CountLetters {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String sentence = input.nextLine();
        int fr = 0;
        List<Character> characters = new ArrayList<Character>();
        Map<Character, Integer> table = new HashMap<Character, Integer>();

        for (int i = 0; i<sentence.length(); i++) {
            if (sentence.charAt(i) != ' ' && sentence.charAt(i) != '.' && sentence.charAt(i) != ',' )
                characters.add(sentence.toLowerCase().charAt(i));

        }
        for (Character character:characters) {
            fr = Collections.frequency(characters, character);
            table.put(character, fr);

        }
        for (Object set:table.keySet() ) {
            System.out.println(set+" "+table.get(set));

        }
    }
}
