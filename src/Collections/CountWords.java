package Collections;

import java.util.*;
//считает кол-во повторений слов в предложении...
public class CountWords {
    public static void main(String[] args)
    {
        List <String> collectionOfWords = new ArrayList<String>();
        Map<String, Integer> map = new HashMap<String, Integer>();
        Scanner sc = new Scanner(System.in);
        int fr;

        System.out.println("enter words");
        String[] text = sc.nextLine().split(" ");

        sc.close();
        // каждое слово как отдельный элемент коллекции List
        for (String tString:text) {
            collectionOfWords.add(tString);
        }

        for (String word:collectionOfWords) {
            // подсчет повторений каждого слова в предложении, без исключений
            fr = Collections.frequency(collectionOfWords, word) ;
            // создание коллекции хэш-мэп содержащей уникальные ключи ( в виде слов) и значений (кол-во повторений)
            if (fr > 1)
                map.put(word, fr);
        }
        // вывод на экран всех ключей и значений хэш мэп.
        for(Object objname:map.keySet()) {
            System.out.println(objname+" "+map.get(objname));
        }



    }
}
