//16.13 (Duplicate Elimination) Write a program that reads in a series of first names and eliminates
//        duplicates by storing them in a Set . Allow the user to search for a first name.
package Collections;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class DuplicateElimination {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Set<String> setCollection = new HashSet<>();

        System.out.println("Enter first name for adding into list, write search to SEARCH for a first name, and CANCEL to exit ");
        String  text = sc.nextLine();


        while(!text.toLowerCase().equals("cancel")){

            if (text.toLowerCase().equals("search")){
                System.out.println("Enter first name");
                text = sc.nextLine();
                if(setCollection.contains(text))
                    System.out.println("First name exists in the collection");
                else
                    System.out.println("First name doesn't exist in the collection");
            }
            else setCollection.add(text);
            text = sc.nextLine();

        }


        for (String word: setCollection) {
            System.out.println(word);

        }
        sc.close();

    }
}
