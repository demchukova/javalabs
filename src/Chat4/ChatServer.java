package Chat4;

import java.security.SecureRandom;

import java.net.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ChatServer {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(8888, 100, InetAddress.getByName("localhost"));
            System.out.println("Server started ...\n");
            while (true) {
                final Socket s = serverSocket.accept();
                System.out.println("New user entered in chat : " + s);
                Runnable runnable =
                        () -> handleClientRequest(s);
                new Thread(runnable).start(); // start a new thread
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void handleClientRequest(Socket socket) {
        BufferedWriter socketWriter = null;
        try {
            socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            String outMsg = getSonet();

            socketWriter.write(outMsg);
            socketWriter.flush();
            Thread.sleep(5000);
            System.out.println("User exited from chat : " + socket);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getSonet() throws IOException {
        return new String(Files.readAllBytes(Paths.get("src/Chat4/sources/" + (new SecureRandom().nextInt(5) + 1) + ".txt")));
    }

}